'use strict';

/* globals module, require */

var validator = require('validator'),
	helpersRoute = require.main.require('./src/routes/helpers'),
	setupPageRoute = helpersRoute.setupPageRoute,

	plugin = {};

plugin.init = function(params, callback) {
	var middlewares = [params.middleware.checkGlobalPrivacySettings];

	setupPageRoute(params.router, '/groups', params.middleware, middlewares, showNotFound);
	setupPageRoute(params.router, '/groups/:slug', params.middleware, middlewares, showNotFound);
	setupPageRoute(params.router, '/groups/:slug/members', params.middleware, middlewares, showNotFound);

	callback();
};

var showNotFound = function(req, res) {
	//return res.redirect('/404');

	var path = String(req.path || '');
	res.render('404', {path: validator.escape(path), title: '[[global:404.title]]'});
};

module.exports = plugin;
